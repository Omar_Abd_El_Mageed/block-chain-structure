const SHA256 = require('crypto-js/sha256');

const EC = require('elliptic').ec;
const ec = new EC('secp256k1');

class Transaction
{
    constructor(fromAdress, toAdress, amount)
    {
        this.fromAdress = fromAdress;
        this.toAdress = toAdress;
        this.amount = amount;
    }

    calculateHash()
    {
        return SHA256(this.fromAdress, this.toAdress, this.amount).toString();
    }

    signTransaction(signingKey) //object we got from the key generator
    
    {
        if(signingKey.getPublic('hex') != this.fromAdress)
        {
            throw new Error('You cannot Send money for other wallets!');
        }
        const hashTx = this.calculateHash();
        const sig = signingKey.sign(hashTx, 'base64');
        this.signature = sig.toDER('hex'); 
    }

    isValid()
    {
        if(this.fromAdress == null) return true; // this will be valid in case of the  minner is one who will recieve the money

        if(!this.signature || this.signature.length == 0) {
            throw new Error('No signature in this account!')
        }

        const publicKey = ec.keyFromPublic(this.fromAdress, 'hex');
        return publicKey.verify(this.calculateHash(), this.signature);
    }
}
class Block
{
    constructor(timestamp, transactions, previoushash = '')
    {
        this.timestamp = timestamp;
        this.transactions = transactions;
        this. previoushash = previoushash;
        this.hash = this.calculateHash();
        this.nonce = 0;
    }

    calculateHash()
    {
        return SHA256(this.previoushash+this.timestamp+JSON.stringify(this.data) + this.nonce).toString();
    }

    mineBlock(difficulty)
    {
        while(this.hash.substring(0, difficulty) !== Array(difficulty + 1).join("0"))
        {
            this.nonce++;
            this.hash = this.calculateHash();
        }
        console.log("Block mined " + this.hash);
    }

    hasValidTransaction()
    {
        for(const tx of this.transactions)
        {
            if(!tx.isValid())  return false;
        }
        return true;
    }
}

class BlockChain
{
    constructor()
    {
        this.chain = [this.createGenesisBlock()];
        this.difficulty = 2;
        this.pendingTransactions = [];
        this.miningReward = 100;
    }

    createGenesisBlock()
    {
        return new Block(Date.parse('2017-01-01'), [], '0');    
    }

    getlatestBlock()
    {
        return this.chain[this.chain.length -1];
    }

    minePendingTransactios(miningRewardAdress)
    {
        const rewardTx = new Transaction(null, miningRewardAdress, this.miningReward);
        this.pendingTransactions.push(rewardTx);
        let block = new Block(Date.now(), this.pendingTransactions);
        block.mineBlock(this.difficulty);

        console.log("Block successfully minded");
        this.chain.push(block);
        this.pendingTransactions = [new Transaction(null, miningRewardAdress, this.miningReward)];
    }

    addTransaction(transaction)
    {
        if(!transaction.fromAdress || !transaction.toAdress)
        {
            throw new Error("Transaction must contain from and to address!");
        }

        if(!transaction.isValid())
        {
            throw new Error("Cannot add invalid transaction to chain!");
        }

        this.pendingTransactions.push(transaction);
    }

    getBalanceOfAddress(address)
    {
        let balance = 0;
        for(const block of this.chain)
        {
            for(const trans of block.transactions)
            {
                if(trans.fromAdress == address)
                {
                    balance -= trans.amount;
                }
                if(trans.toAdress == address)
                {
                    balance += trans.amount;
                }
            }
        }
        return balance;
    }

    isChainValid()
    {
        for(let i = 1; i < this.chain.length; i++)
        {
            const currentBlock = this.chain[i];
            const previousBlock = this.chain[i-1];

            if(!currentBlock.hasValidTransaction())                return false;

            if(currentBlock.hash !== currentBlock.calculateHash()) return false;

            if(currentBlock.previoushash !== previousBlock.hash)   return false;
        }
        return true;
    }
}




module.exports.BlockChain = BlockChain;
module.exports.Transaction = Transaction;