const {BlockChain, Transaction} = require('./blockchain')
const EC = require('elliptic').ec;
const ec = new EC('secp256k1');

const myKey = ec.keyFromPrivate('5e1c0a637cc944dafc735918bc73cfef4c176454f98a1d3a27c0fc2130580f08')
const myWalletAdress = myKey.getPublic('hex');


let myCoin  = new BlockChain();

const tx1 = new Transaction(myWalletAdress, 'the actual public key should be here', 33); // the minner wallet will be 100 - 33 = 67
tx1.signTransaction(myKey);
myCoin.addTransaction(tx1); 

console.log('\n Starting the minner ........');
myCoin .minePendingTransactios(myWalletAdress);  //the miner who will recieve the mining reward

console.log('\nBalance of the minner is : ', myCoin.getBalanceOfAddress(myWalletAdress));



