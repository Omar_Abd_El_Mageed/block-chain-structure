const EC = require('elliptic').ec;
const ec = new EC('secp256k1');

const key = ec.genKeyPair();

const publicKey = key.getPublic('hex');
console.log("Public Key is :: ", publicKey);

const privateKey = key.getPrivate('hex');
console.log("Private Key is :: ", privateKey);
